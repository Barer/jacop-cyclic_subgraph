package org.jacop.constraints;

import org.jacop.api.SatisfiedPresent;
import org.jacop.core.IntDomain;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;

import java.util.*;

public class CyclicSubgraph extends Constraint implements SatisfiedPresent {

    /* Quick implementation of union-find data structure */
    protected static class UnionFind<T> {
        protected Map<T, T> parents;

        public UnionFind(Collection<T> keys) {
            this.parents = new HashMap<>();
            for (T key : keys) {
                this.parents.put(key, key);
            }
        }

        public T find(T val) {
            T prevval;
            do {
                prevval = val;
                val = this.parents.get(prevval);
            }
            while(val != prevval);
            return val;
        }

        public void union(T val1, T val2) {
            T baseVal1 = this.find(val1);
            T baseVal2 = this.find(val2);
            this.parents.put(baseVal1, baseVal2);
        }

        public int numDifferent() {
            HashSet<T> distinct = new HashSet<>();
            for (T val : this.parents.values()) {
                distinct.add(val);
            }
            return distinct.size();
        }
    }

    // List of variables which were changed and need to be revisited
    LinkedHashSet<IntVar> variableQueue = new LinkedHashSet<IntVar>();

    // Graph representation
    protected Map<IntVar, List<IntVar>> node_to_edges;
    protected Map<IntVar, List<IntVar>> edge_to_nodes;

    /**
     * CyclicSubgraph assures that (unoriented) subgraph G' of (unoriented) graph G is cyclic where
     * - G = (V, E); V = [1..N], |E|=M
     * - G' = (V', E')
     * - from, to = array[1..M] of 1..N: forall i: (from[i],to[i]) \in E
     * - ns = array[1..N] of bool: ns[i] == true <-> i \in V'
     * - es = array[1..M] of bool: es[i] == true <-> i \in E'
     *
     * @param from Array of indexes of vertices in edges.
     * @param to   Array of indexes of vertices in edges.
     * @param ns   Array of decision boolean variables, whether given vertex is in the subgraph.
     * @param es   Array of decision boolean variables, whether given edge is in the subgraph
     */
    public CyclicSubgraph(
            int[] from,
            int[] to,
            IntVar[] ns,
            IntVar[] es
    ) {
        // Set this contraint's priority
        // See DecomposedConstraint.queueIndex
        this.queueIndex = 4;

        // Preprocess contraint
        int node_count = ns.length;
        int edge_count = es.length;

        if (from.length != edge_count || to.length != edge_count) {
            throw new IllegalArgumentException("Expected \"from\" and \"to\" to be equal size as \"es\".");
        }

        this.node_to_edges = new HashMap<IntVar, List<IntVar>>();
        this.edge_to_nodes = new HashMap<IntVar, List<IntVar>>();

        // Initial empty node->edge map
        for (int i = 0; i < node_count; i++) {
            this.node_to_edges.put(ns[i], new ArrayList<>(4));
        }

        // Iterate through all edges
        for (int i = 0; i < edge_count; i++) {
            int from_vertex = from[i];
            int to_vertex = to[i];

            if (from_vertex < 1 || from_vertex > node_count || to_vertex < 1 || to_vertex > node_count) {
                throw new IllegalArgumentException("Expected \"from\" and \"to\" values to be in range of size of \"ns\".");
            }

            IntVar e = es[i];
            IntVar v1 = ns[from_vertex - 1];
            IntVar v2 = ns[to_vertex - 1];

            // Add edge
            List<IntVar> edge_nodes = new ArrayList<IntVar>(2);
            edge_nodes.add(v1);
            edge_nodes.add(v2);
            this.edge_to_nodes.put(e, edge_nodes);

            // Add edge to nodes
            this.node_to_edges.get(v1).add(e);
            this.node_to_edges.get(v2).add(e);
        }

        // Set variable scope
        List<Var> vars = new ArrayList<>();
        vars.addAll(this.node_to_edges.keySet());
        vars.addAll(this.edge_to_nodes.keySet());
        setScope(vars.stream());
    }

    // Keeps consistency on nodes (0 or 2 neighbouring edges)
    protected void consistencyOnNodeRank(Store store, List<IntVar> variablesToCheck)
    {
        // When checking edge, always check nodes
        // When checking node, check only edges that were changed
        // -> Everything is eventually checked and there are no infinite loops

        // We can possibly add another variables to be checked,
        // so we cannot use foreach (mutability)
        while (!variablesToCheck.isEmpty()) {
            // Get the variable, remove all further instances (needed to be checked only once)
            IntVar var = variablesToCheck.get(0);
            while(variablesToCheck.remove(var));

            // If variable is a node
            if (this.node_to_edges.containsKey(var)) {
                if (var.singleton(0)) {
                    // If node is not present, no edge is present
                    for (IntVar edge : this.node_to_edges.get(var)) {
                        // Undecided (ok) or present (will raise an exception = ok)
                        if (!edge.singleton(0)) {
                            edge.domain.in(store.level, edge, 0, 0);
                            variablesToCheck.add(edge);
                        }
                    }
                } else {
                    // If node may be present, count possibility range
                    boolean mustBePresent = var.singleton(1);
                    int minPossible = 0;
                    int maxPossible = 0;
                    for (IntVar edge : this.node_to_edges.get(var)) {
                        // Undecided or present
                        if (!edge.singleton(0)) {
                            maxPossible++;
                        }
                        // Present
                        if (edge.singleton(1)) {
                            minPossible++;
                        }
                    }

                    // If there are too much edges
                    if (minPossible > 2) {
                        // Not satisfiable
                        var.domain.in(store.level, var, -1, -1);
                    }
                    // If there are less than 2 edges
                    else if (maxPossible < 2) {
                        // Set this node as not present - edges will be removed in another run
                        // If node is present, then this will throw an exception - ok
                        var.domain.in(store.level, var, 0, 0);
                        variablesToCheck.add(var);
                    }
                    // If there are 2 or less for present node
                    else if (mustBePresent && minPossible < 2 && maxPossible == 2) {
                        // There must be exactly 2 edges, not less
                        for (IntVar edge : this.node_to_edges.get(var)) {
                            // If decided, we cannot modify it
                            // But we know that rest of the edges need to be present
                            if (!edge.singleton()) {
                                edge.domain.in(store.level, edge, 1, 1);
                                variablesToCheck.add(edge);
                            }
                        }
                    }
                    // If there are 2 or more
                    else if (minPossible == 2 && maxPossible > 2) {
                        // There muse be exactly 2 edges, not more
                        for (IntVar edge : this.node_to_edges.get(var)) {
                            // If decided, we cannot modify it
                            // But we know that rest of the edges need not to be present
                            if (!edge.singleton()) {
                                edge.domain.in(store.level, edge, 0, 0);
                                variablesToCheck.add(edge);
                            }
                        }
                        // Also, set this node as present
                        if (!var.singleton()) {
                            var.domain.in(store.level, var, 1, 1);
                            variablesToCheck.add(var);
                        }
                    }
                }
            }
            // If edge
            else if (this.edge_to_nodes.containsKey(var)) {
                // If edge is present, both nodes must be present
                if (var.singleton(1)) {
                    for (IntVar node : this.edge_to_nodes.get(var)) {
                        if (!node.singleton(1)) {
                            node.domain.in(store.level, node, 1, 1);
                            variablesToCheck.add(node);
                        }
                    }
                }
                // Else if edge was removed, we can check remaining nodes
                // (if there are enough edges possible for the node)
                else if (var.singleton(0)) {
                    for (IntVar node : this.edge_to_nodes.get(var)) {
                        if (!node.singleton(0)) {
                            variablesToCheck.add(node);
                        }
                    }
                }
                // Else edge may or may not be present, we cannot do anything better
            }
        }
    }

    // Check if there is a cycle that does not contain all edges
    private void consistencyOnOneCycle(Store store) {
        HashSet<IntVar> unchecked = new HashSet<IntVar>(this.node_to_edges.keySet());
        boolean hasAnyCycle = false;
        boolean hasAnyNoncycle = false;

        while (!unchecked.isEmpty()) {
            // Get an unchecked node
            IntVar root = unchecked.stream().findFirst().get();

            // If it is not decided as present, we skip it
            if (!root.singleton(1)) {
                unchecked.remove(root);
                continue;
            }

            // Start search for a chain
            List<IntVar> currentChain = new ArrayList<IntVar>();
            currentChain.add(root);
            IntVar lastNode = root;
            IntVar lastEdge = null;
            while (true) {
                // Find following edge in chain
                IntVar nextEdge = null;
                for (IntVar edge : this.node_to_edges.get(lastNode)) {
                    if (edge.singleton(1) && edge != lastEdge) {
                        nextEdge = edge;
                        break;
                    }
                }

                // If chain ended
                if (nextEdge == null) {
                    // Mark that we have a chain (if this is not an unconnected node)
                    hasAnyNoncycle = hasAnyNoncycle || (lastEdge != null);
                    // Check if consistent
                    if (hasAnyCycle && hasAnyNoncycle) {
                        // Not satisfiable
                        root.domain.in(store.level, root, -1, -1);
                    }
                    // Else we checked all nodes in current chain
                    unchecked.removeAll(currentChain);
                    break;
                }

                // Find following node that is unchecked
                // (we could get into already checked chain)
                // (rank 0/2 is expected to be checked)
                IntVar nextNode = null;
                for (IntVar node : this.edge_to_nodes.get(nextEdge)) {
                    if (node != lastNode && unchecked.contains(node)) {
                        nextNode = node;
                        break;
                    }
                }

                // If we are in already checked node
                if (nextNode == null) {
                    // Mark that we have a chain
                    hasAnyNoncycle = true;
                    // Check if consistent
                    if (hasAnyCycle && hasAnyNoncycle) {
                        // Not satisfiable
                        root.domain.in(store.level, root, -1, -1);
                    }
                    // Else we checked all nodes in current chain
                    unchecked.removeAll(currentChain);
                    break;
                }

                // If we are back in root, we have a cycle
                if (nextNode == root) {
                    // Check if consistent
                    if (hasAnyCycle && hasAnyNoncycle) {
                        // Not satisfiable
                        root.domain.in(store.level, root, -1, -1);
                    }
                    // Mark that we have a cycle
                    hasAnyCycle = true;
                    // Else we checked all nodes in current cycle
                    unchecked.removeAll(currentChain);
                    break;
                }

                // Else we continue
                currentChain.add(nextNode);
                lastEdge = nextEdge;
                lastNode = nextNode;
            }
        }
    }

    @Override
    public void consistency(Store store) {
        // Check every variable every time
        List<IntVar> variablesToCheck = new ArrayList<IntVar>(this.edge_to_nodes.size() + this.node_to_edges.size());
        for (IntVar edge : this.edge_to_nodes.keySet()) {
            variablesToCheck.add(edge);
        }
        for (IntVar node : this.node_to_edges.keySet()) {
            variablesToCheck.add(node);
        }
        this.consistencyOnNodeRank(store, variablesToCheck);

        // Check if there is a cycle that does not contain all edges
        this.consistencyOnOneCycle(store);
    }

    @Override
    public int getDefaultConsistencyPruningEvent() {
        // Force consistency check only if new singleton was made,
        // else we will make expensive component check
        return IntDomain.GROUND;
    }

    @Override
    public void queueVariable(int level, Var var) {
        variableQueue.add((IntVar) var);
    }

    @Override
    public boolean satisfied() {
        // Check nodes
        for (IntVar node : this.node_to_edges.keySet()) {
            // Needs to be specified
            if (!node.singleton()) {
                return false;
            }
            // Needs correct edges
            int edgeCount = 0;
            for (IntVar edge : this.edge_to_nodes.keySet()) {
                // Needs to be specified
                if (!edge.singleton()) {
                    return false;
                }
                if (edge.singleton(1)) {
                    edgeCount++;
                }
            }
            if (!(
                    (node.singleton(0) && edgeCount == 0) ||
                    (node.singleton(1) && edgeCount == 2)
            )) {
                return false;
            }
        }

        // Needs to be in one component
        UnionFind<IntVar> componentsOfVertices = new UnionFind<IntVar>(this.node_to_edges.keySet());
        for (IntVar edge : this.edge_to_nodes.keySet()) {
            if (edge.singleton(1)) {
                List<IntVar> nodes = this.edge_to_nodes.get(edge);
                componentsOfVertices.union(nodes.get(0), nodes.get(1));
            }
        }

        return componentsOfVertices.numDifferent() == 1;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(": CyclicSubgraph([Nodes=[");

        for (IntVar var : this.node_to_edges.keySet()) {
            result.append(var).append(" ");
        }

        result.append(("], Edges=["));

        for (IntVar var : this.edge_to_nodes.keySet()) {
            result.append(var).append(" ");
        }

        result.append(("]])"));

        return result.toString();
    }
}
